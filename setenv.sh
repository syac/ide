#!/bin/bash

#
# Set your shell environment
#
#

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export SYAC_HOME="${SCRIPT_DIR}/.ide/syac"
export SYAC_REPO="${SCRIPT_DIR}"
export PATH=$PATH:${SCRIPT_DIR}:${SYAC_HOME}/bin:${SCRIPT_DIR}/.ide
