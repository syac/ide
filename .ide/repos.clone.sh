#!/bin/bash

#
# Starter Kit Bootstrap
#
# Author : Nomane Oulali
#


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}/..""

REPOS_REGISTRY="${PROJECT_DIR}/.ide/custom/git.repos"
REPOS_REGISTRY_CUSTOM="${PROJECT_DIR}/git.repos.projects"

# cloning from GIT repos
function clone {
  while IFS='' read -r REPO_URL || [[ -n "$REPO_URL" ]]; do
    if [[ ! "$REPO_URL" =~ ^#.* ]]; then
      REPO_NAME=$(basename "$REPO_URL" ".${REPO_URL##*.}") # ignore comment
      if [ ! -d "$REPO_NAME" ]; then
        tput setaf 2
        echo "Clone $REPO_NAME from $REPO_URL"
        tput setaf 7
        git clone $REPO_URL
      fi
    fi
  done < $1
}

cd $PROJECT_DIR

# base repos
if [ -f "$REPOS_REGISTRY" ]; then
  clone $REPOS_REGISTRY
fi

# custom repos
if [ -f "$REPOS_REGISTRY_CUSTOM" ]; then
  clone $REPOS_REGISTRY_CUSTOM
fi
