#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}/..""

. ${SCRIPT_DIR}/common.sh

echo -e "${NOTICE_FLAG} Pulling IDE ...$WHITE"
cd ${PROJECT_DIR}/.ide
git pull origin master --rebase

echo -e "${NOTICE_FLAG} Pulling IDE Settings ...$WHITE"
cd ${PROJECT_DIR}/.ide/custom
git pull origin master --rebase
