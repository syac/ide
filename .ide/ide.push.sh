#!/bin/bash

# Default commit message
COMMIT_MSG="incremental"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}/..""

. ${SCRIPT_DIR}/common.sh

# Compute args
while getopts "m:" opt; do
    case "$opt" in
      m)  COMMIT_MSG=$OPTARG
          ;;
      esac
done

echo -e "${NOTICE_FLAG} Pushing IDE ...$WHITE"
cd ${PROJECT_DIR}/.
git add .  && git commit -m "$COMMIT_MSG"  && git push -u origin master


echo -e "${NOTICE_FLAG} Pushing IDE Settings ...$WHITE"
cd ${PROJECT_DIR}/.ide/custom
git add .  && git commit -m "$COMMIT_MSG"  && git push -u origin master
