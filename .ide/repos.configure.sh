#!/bin/bash

#
# Starter Kit Bootstrap
#
# Configure all cloned repo with content of template/
# Do not use this script if you are not sure of its purpose
#
# Param: No param mean ALL GIT directory otherwise give the directory to configure
#
# Author : Nomane Oulali
#

#set -x

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}""
TEMPLATE_PATH="${SCRIPT_DIR}/custom/template"

if [ "$#" -eq 0 ]; then
    # ALL GIT directory
    for dir in `ls ${SCRIPT_DIR}/..`
    do
      if [ -d $dir ]; then
        cd $dir
        # only git repo
        if [ -d ".git" ]; then
          tput setaf 2
          echo "Configure $dir"
          tput setaf 7
          read -p "   Are your sure to overwrite all configuration ? " yn
          case $yn in
              [Yy]* ) cp -r ${TEMPLATE_PATH}/. . && echo "   done";;
              [Nn]* ) exit;;
              * ) echo "Please answer yes or no.";;
          esac
        fi
        cd ..
      fi
    done
else
    for REPO in "$@"
    do
      if [ -d "$REPO" ]; then
        tput setaf 2
        echo "Configure $REPO"
        tput setaf 7
        cd $REPO
        read -p "   Are your sure to overwrite all configuration ? " yn
        case $yn in
            [Yy]* ) cp -r ${TEMPLATE_PATH}/. . && echo "   done";;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
        cd ..
      else
        echo "Directory $REPO does not exists"
        exit 2
      fi
    done
fi
