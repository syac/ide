#!/bin/bash

#
# Syac IDE install
#
# usage: install.sh [--nosyac] [--force] [--ide CUSTOM_IDE_REPO_LOCATION]
#   --nosyac : disable Syac install
#   --force : for new version pulling by deleting your local IDE change
#
# Below the installation step:
#   $ git config --global core.eol lf
#   $ git config --global core.autocrlf input
#   $ git clone https://nomane_lmg_iac@bitbucket.org/syac/ide.git ./
#   $ . setenv.sh
#   $ install.sh --ide https://nomane_lmg_iac@bitbucket.org/nomane_lmg_iac/socle_lmg_iac_ide.git
#
#
#
# Author : Nomane Oulali
#

# set -x

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}""

# By default install do not install the engine
ENABLE_SYAC=false

# Force override local IDE change
FORCE_INSTALL=false

# Specific settings
CUSTOM_IDE_REPO_LOCATION=""

# Compute args
while true; do
  case "$1" in
    --withsyac ) ENABLE_SYAC=true; shift;;
    --force ) FORCE_INSTALL=true; shift;;
    --ide ) CUSTOM_IDE_REPO_LOCATION=$2; shift; shift;;
    * ) break ;;
  esac
done

cd ${PROJECT_DIR}

./setenv.sh

# Get latest IDE framework
tput setaf 5
echo "Checking for latest IDE ..."
tput setaf 7
if [ $FORCE_INSTALL == true ]; then
  # Force overwrite of IDE local changes
  echo -ne "Force IDE update, notice that all you local change on IDE will be lost. "
  read -p "Are you sure? " -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    git fetch --all
    git reset --hard origin/master
    git pull --rebase
  else
    # Cancel operation
    exit
  fi
else
  # Soft update
  git pull
fi

# Get latest custom IDE settings
if [ ! -d ${PROJECT_DIR}/.ide/custom ]; then
  tput setaf 5
  echo "Installing custom IDE settings ..."
  tput setaf 7
  if [ -n "$CUSTOM_IDE_REPO_LOCATION" ]; then
    mkdir -p ${PROJECT_DIR}/.ide/custom
    cd ${PROJECT_DIR}/.ide/custom
    git clone ${CUSTOM_IDE_REPO_LOCATION} ./
  else
    echo "Please, provide the GIT repo where your IDE settings will be saved"
  fi
else
  tput setaf 5
  echo "Update custom IDE settings ..."
  tput setaf 7
  cd ${PROJECT_DIR}/.ide/custom
  git pull
fi

if [ $ENABLE_SYAC == true ]; then
  # Syac
  if [ ! -d "$SYAC_HOME" ]; then
    # Install Syac
    tput setaf 5
    echo "Installing framework ..."
    tput setaf 7
    git clone https://syac@bitbucket.org/syac/syac.git ${PROJECT_DIR}/.ide/syac
  else
    # Syac is installed, get latest change
    tput setaf 5
    echo "Updating framework ..."
    tput setaf 7
    cd ${SYAC_HOME}
    git pull --rebase
  fi
  pip install -r ${SYAC_HOME}/requirements.txt
fi

# Clone registred GIT repos
cd ${PROJECT_DIR}
tput setaf 5
echo "Cloning projects ..."
tput setaf 7
${PROJECT_DIR}/.ide/repos.clone.sh
