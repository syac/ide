#!/bin/bash

#
# IDE Bootstrap
#
# Push to GIT
#
# Usage : git.push.sh [-m 'your commit message'] [-all] [Repo1 Repo2]
#
# Note : For repos list you can give regexp as well since SHELL expand it
#
# Author : Nomane Oulali
#

# Default commit message
COMMIT_MSG="incremental"

#
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR=""${SCRIPT_DIR}""
CUR_DIR=`pwd`
ALL_REPO=false

echo $CUR_DIR

# Compute args
while true; do
  case "$1" in
    -m | --message ) COMMIT_MSG=$2; shift; shift ;;
    -a | --all ) ALL_REPO=true; shift ;;
    * ) break ;;
  esac
done

function pushToGit {
  tput setaf 2
  echo
  echo "Push $1"
  tput setaf 7
  git add . && git commit -m "$COMMIT_MSG"
  # git push -u origin master
  git push -u origin
}

tput setaf 5
echo
echo "Start pushing updates with message: '$COMMIT_MSG'"
echo
tput setaf 7

if [ $ALL_REPO == true ]; then
    # ALL GIT directory
    read -p "Are your sure to push from *ALL* GIT folder in this directory ? " yn
    case $yn in
        [Yy]* ) echo "Let's go ...";;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
    for dir in `ls`
    do
      if [ -d $dir ]; then
        cd $dir
        # only git repo
        if [ -d ".git" ]; then
          pushToGit $dir
        fi
        cd ..
      fi
    done
elif [ $# == 0 ]; then
    # Push current directory
    if [ ! -d ".git" ]; then
      echo "Current directory is not a GIT repo"
      exit 2
    fi
    pushToGit .
else
    # User give repos list, iterate over it
    for REPO in "$@"
    do
      if [ -d "$REPO" ]; then
        cd $REPO
        pushToGit $REPO
        cd ..
      else
        echo "Directory $REPO does not exists"
        exit 2
      fi
    done
fi
